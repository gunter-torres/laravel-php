<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('gestion', 'GestionController');

Route::get('/gestiones','GestionClienteController@index')->name('gestiones');
Route::post('/gestion/add/','GestionClienteController@agregar')->name('gestionAdd');

Route::get('/ticket','TicketController@index')->name('ticket');
Route::post('/ticket/get/{atender?}','TicketController@lista')->name('listado');
Route::post('/ticket/save','TicketController@guardar')->name('guardar');
