<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_gestion');
            $table->unsignedBigInteger('id_gestion_cliente');
            $table->string('nombre_cliente')->default('');
            $table->string('apellido_cliente')->default('');
            $table->longText('direccion_cliente');
            $table->string('telefono_cliente')->default('');
            $table->longText('problema');
            $table->longText('solucion');
            $table->timestamps();
            $table->foreign('id_gestion')
                ->references('id')
                ->on('gestiones')
                ->onDelete('cascade');
            $table->foreign('id_gestion_cliente')
                ->references('id')
                ->on('gestion_clientes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}

