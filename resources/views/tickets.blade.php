@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-12 text-center">
        <h3>
            Tickets
        </h3>
    </div>
    <div class="col-md-12">
        <h3 id="encabezado">

        </h3>
    </div>
    <div class="row">
        <div class="col-md-8">

            <div id="error">
            </div><br />

            <form id="form">
                @csrf
                <input type="hidden" name="id" id="id">
                <div class="row">
                    <div class="col-md-6">
                        Nombre
                        <input name="nombre" type="text" class="form-control" required>

                    </div>
                    <div class="col-md-6">
                        Apellido
                        <input name="apellido" type="text" class="form-control" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        Dirección
                        <input name="direccion" type="text" class="form-control" required>

                    </div>
                    <div class="col-md-6">
                        Teléfono
                        <input name="telefono" type="text" class="form-control" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        Gestión real realizada
                        <select name="gestion" id="" class="form-control" required>
                            <option value="">Seleccione</option>
                            @foreach ($gestion as $fila)
                            <option value="{{$fila->id}}">{{$fila->nombre}}</option>
                            @endforeach
                        </select>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="font-weight-bold">Problema expuesto por el cliente</p>
                        <input name="problema" type="text" class="form-control" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="font-weight-bold">Solución brindada</p>
                        <input name="solucion" type="text" class="form-control" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 offset-md-4">
                        <button type="submit" id="save" class="btn btn-primary form-control">
                            Guardar Gestión
                        </button>
                    </div>
                </div>
            </form>

        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-5 offset-md-1">
                    <button class="btn btn-sm btn-info" id="atender">Atender al cliente</button>
                </div>
                <div class="col-md-6">
                    <button class="btn btn-sm btn-info" id="actualiza">Actualizar lista de tickets</button>
                </div>
            </div>
            <br>
            <div class="row">
                <table class="table table-sm table-bordered ">
                    <thead class="thead-light">

                        <tr>
                            <th>No. Ticket</th>
                            <th>Gestion Solicitada</th>
                        </tr>
                    </thead>
                    <tbody id="listado">
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<script>
    function pad(n) { return ("000000" + n).slice(-6); }

    $(document).ready(function(){

        $("#save").css('display','none');
        form.reset();

        function listado(num){
            url = "{{route('listado')}}";
            if(1 == num) {
                url += "/1"
            }

            $.ajax({
                url: url,
                type: "POST",
                data: {
                    _token: '{{Session::token()}}'
                },
                cache: false,
                success: function(dataResult){
                    console.log(dataResult);
                    data = JSON.parse(dataResult);

                    if(0 == num ){
                        $("#listado").html("");

                        $.each(data, function (key, value) {

                            row = '<tr>';
                            row += '<td>' + value.id + '</td>';
                            row += '<td>' + value.gestion + '</td>';
                            row += '</tr>';

                            $('#listado').append(row);
                        });
                    }else {
                        str = "Ticket #" + pad(data[0].id);
                        str += " " + data[0].gestion;

                        $('#id').val(data[0].id);
                        $('#encabezado').append(str);
                        $("#save").css('display','block');

                    }

                }
            });
        }

        listado(0);

        $("#actualiza").on('click', function() {
            listado(0);
        });

        $("#atender").on('click', function() {
            listado(1);
            form.reset();
        });

        $( "form" ).on( "submit", function(e) {
            var dataString = $(this).serialize();

            $.ajax({
                type: "POST",
                url: "{{ route('guardar') }}",
                data: dataString,
                success: function (res) {
                    console.log(res);
                    var dataResult = JSON.parse(res);

                        if(dataResult.statusCode==200){
                            alert("Ticket agregada");
                            window.location.reload();
                        }

                },
                error: function (res) {
                    $('#error').html('');
                    $.each(res.responseJSON.errors, function(key,value) {
                        $('#error').append('<div class="alert alert-danger">'+value+'</div');
                    });
                },
            });

            e.preventDefault();
        });

    });

</script>

@endsection
