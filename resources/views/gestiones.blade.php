@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-12 text-center">
        <h3>
            Gestiones
        </h3>
        <div class="row">
            @foreach ($data as $fila)
            <div class="col-sm-6">
                <button class="btn btn-primary btn-lg form-control m-2 add" id="{{$fila->id}}">
                    {{--<a href="{{ route('ticketAdd', $fila->id)}}"
                    role="button">--}}
                    {{$fila->nombre}}
                </button>

            </div>
            @endforeach

        </div>


    </div>

    <script>
        $(document).ready(function(){

            $('.add').on('click', function() {

                var id = $(this).attr('id');

                $.ajax({
                    url: "{{ route('gestionAdd')}}",
                    type: "POST",
                    data: {
                        _token: '{{Session::token()}}',
                        id: id
                    },
                    cache: false,
                    success: function(dataResult){
                        //console.log(dataResult);

                        var dataResult = JSON.parse(dataResult);

                        if(dataResult.statusCode==200){
                            alert("Gestión agregada");
                        }else{
                            alert("¡Error. Consulte con el administrador del sistema!");
                        }
                    //*/
                    }
                });
            //*/
            });

        });

    </script>

    @endsection
