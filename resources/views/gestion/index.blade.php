@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-12 text-center">
        <h3>
            Catálogo de Gestión
        </h3>

        <a href="{{ route('gestion.create') }}" class="btn btn-primary btn-lg " role="button">
            Agregar
        </a>
    </div>
    @if(session()->get('mensaje'))
    <div class="alert alert-success">
        {{ session()->get('mensaje') }}
    </div><br />
    @endif
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Visita técnica</th>
                <th colspan="2">Opciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $fila)
            <tr>
                <td>{{$fila->nombre}}</td>
                <td> {{empty($fila->visita)?'No':'Si'}} </td>
                <td>

                    <a href="{{ route('gestion.edit', $fila->id)}}" class="btn btn-success">
                        Editar
                    </a>

                </td>
                <td>
                    <form action="{{ route('gestion.destroy', $fila->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="button" class="btn btn-danger"
                            onclick="return confirmar(this.form)">Eliminar</button>
                    </form>

                </td>
            </tr>
            @endforeach

            @if ($data->count()<=0) <tr>
                <td colspan="3" class="text-center">Níngun registro para mostrar</td>
                </tr>
                @endif
        </tbody>

    </table>

    <div class="justify-content-center">
        {{ $data->links() }}
    </div>
</div>

<script>
    function confirmar(frm){
        if(confirm('¿Desea eliminar el registro?')){
            frm.submit();
        }
    }

</script>

@endsection
