@extends('layouts.app')

@section('content')
<div class="container">

    <div class="card uper">
        <form method="post" action="{{ route('gestion.update',$data->id) }}">
            <div class="card-header text-center">
                Editar Gestión
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif

                <div class="form-group">
                    @csrf
                    @method('PATCH')
                    <label for="nombre">Nombre:</label>
                    <input type="text" name="nombre" class="form-control" required="required"
                    value="{{$data->nombre}}" />
                </div>
                <div class="form-check">
                    <input type="checkbox" name="visita" class="form-check-input" id="visita" value="1"
                    @if( 1==(int)$data->visita)
                    checked
                    @endif
                    >

                    <label class="form-check-label" for="visita">Visita técnica</label>
                </div>

            </div>

            <div class="card-footer text-center">
                <button type="submit" class="btn btn-primary">Guardar</button>

                <a href="{{ route('gestion.index') }}" class="btn btn-danger " role="button">
                    Cancelar
                </a>
            </div>
        </form>
    </div>
</div>
@endsection
