<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gestion;
use App\GestionCliente;

use App\Ticket;

class TicketController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Agrega tickets desde la pantalla de Gestiones
     *
     * @param int $id Id de la gestión
     * @return void
     */
    public function agregar(Request $request)
    {

        $id = $request->input('id');

        $ticket = new Ticket();

        $ticket->id_gestion = $id;
        $ticket->direccion_cliente = '';
        $ticket->problema = '';
        $ticket->solucion = '';

        $ticket->save();

        $ticket->GestionCliente()->create();

        return json_encode(["statusCode" => 200]);
    }

    public function guardar(Request $request)
    {

        $request->validate([
            'nombre' => 'required|max:255',
            'apellido' => 'required|max:255',
            'direccion' => 'required',
            'telefono' => 'required|max:255',
            'gestion' => 'required',
            'problema' => 'required',
            'solucion' => 'required'
        ]);

        $data = [
            'id_gestion' => $request->input('gestion'),
            'id_gestion_cliente' => $request->input('id'),
            'nombre_cliente' => $request->input('nombre'),
            'apellido_cliente' => $request->input('apellido'),
            'direccion_cliente' => $request->input('direccion'),
            'telefono_cliente' => $request->input('telefono'),
            'problema' => $request->input('problema'),
            'solucion' => $request->input('solucion')
        ];

        Ticket::create($data);

        return json_encode(["statusCode" => 200]);
    }

    public function index()
    {
        $gestion = new Gestion();

        return view('tickets', [
            'gestion' => $gestion->get()
        ]);
    }

    public function lista($atender = false)
    {
        $gestiones = new GestionCliente();
        $limite = 10;

        if ($atender) {
            $limite = 1;
        }

        $atenciones = $gestiones->where('atendido', '=', '0')
            ->orderBy('id')
            ->limit($limite)
            ->get();

        $data = [];

        foreach ($atenciones as $fila) {
            $data[] = [
                'id' => $fila->id,
                'gestion' => $fila->gestion->nombre
            ];
        }

        if ($atender) {
            $gestiones->where('id', $data[0]['id'])
                ->update(['atendido' => 1]);
        }

        return json_encode($data);
    }
}
