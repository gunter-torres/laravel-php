<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gestion;
use Facade\Ignition\Solutions\GenerateAppKeySolution;
use Illuminate\Support\Facades\Auth;

class GestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = new Gestion();
        return view('gestion.index', [
            'data' => $data->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('gestion.nuevo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valido = $request->validate([
            'nombre' => 'required|max:255',
        ]);

        $valido['visita'] = empty($request->input('visita')) ? 0 : 1;
        $valido['id_user']  = Auth::id();

        Gestion::create($valido);

        return redirect('/gestion')->with('mensaje', 'Registro almacenado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Gestion::findOrFail($id);

        return view('gestion.editar', [
            'data' => $data
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valido = $request->validate([
            'nombre' => 'required|max:255'
        ]);
        $valido['visita'] = empty($request->input('visita')) ? 0 : 1;
        $valido['id_user']  = Auth::id();

        Gestion::whereId($id)->update($valido);
        return redirect('/gestion')->with('mensaje', 'Registro actualizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = Gestion::findOrFail($id);
        $data->delete();

        return redirect('/gestion')->with('mensaje', 'Registro eliminado');
    }
}
