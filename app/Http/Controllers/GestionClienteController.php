<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gestion;
use App\GestionCliente;

class GestionClienteController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Agrega tickets desde la pantalla de Gestiones
     *
     * @param int $id Id de la gestión
     * @return void
     */
    public function agregar(Request $request)
    {

        $id = $request->input('id');

        $gestion = new GestionCliente();
        $gestion->id_gestion = $id;
        $gestion->save();

        return json_encode(["statusCode" => 200]);
    }

    public function index()
    {
        $data = new Gestion();

        return view('gestiones', [
            'data' => $data->get()
        ]);
    }
}
