<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{

    protected $table = 'tickets';

    protected $fillable = [
        'id_gestion',
        'id_gestion_cliente',
        'nombre_cliente',
        'apellido_cliente',
        'direccion_cliente',
        'telefono_cliente',
        'problema',
        'solucion',
    ];
    //

    public function gestion()
    {
        return $this->belongsTo(Gestion::class);
    }

    public function gestionCliente()
    {
        return $this->belongsTo(GestionCliente::class);
    }
}
