<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gestion extends Model
{

    protected $table = 'gestiones';

    protected $fillable = [
        'nombre', 'visita', 'id_user'
    ];
    //
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function gestionCliente(){
        return $this->hasMany(GestionCliente::class,'id_gestion');
    }


}
