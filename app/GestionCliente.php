<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GestionCliente extends Model
{
    //
    protected $table = 'gestion_clientes';

    protected $fillable = [
        'id_gestion',
        'atendido',
    ];

    public function gestion(){
        return $this->belongsTo(Gestion::class,'id_gestion');
    }

    public function ticket(){
        $this->hasMany(Ticket::class, 'id_gestion_cliente');
    }

}
